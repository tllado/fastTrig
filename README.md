**fastTrig** is a very small library that calculates basic trig functions as quickly as possible on a unsigned integer scale. It is designed for use with microcontrollers or in other applications where floating point math is unavailable or undesirable.  

This includes:  

function|return
:---|:---
`sin8(x)`|128 + 127×sine(2πx/256)
`cos8(x)`|128 + 127×cosine(2πx/256)
`tan8(x)`|128 + 3×tangent(2πx/256)
`sin16(x)`|32768 + 32767×sine(2πx/65536)
`cos16(x)`|32768 + 32767×cosine(2πx/65536)
`tan16(x)`|32768 + 799×tangent(2πx/65536)

This enables a value *a* to be scaled by *sine(x)* by typing  
`uint32_t b = a*(sin8(x)-128)/255;`  
or  
`uint32_t b = a*(sin16(x)-128)/65535;`  

|8-bit functions|16-bit functions|
|:---:|:---:|
|![alt text](https://github.com/tllado/web-content/raw/master/fastTrig/funcs8.jpg "8-bit functions")|![alt text](https://github.com/tllado/web-content/raw/master/fastTrig/funcs16.jpg "16-bit functions")|
|[(high res)](https://github.com/tllado/web-content/raw/master/fastTrig/funcs8big.png)|[(high res)](https://github.com/tllado/web-content/raw/master/fastTrig/funcs16big.png)|
